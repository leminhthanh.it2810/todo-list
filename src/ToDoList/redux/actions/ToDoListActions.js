import {
  add_task,
  delete_task,
  done_task,
  edit_task,
} from "../types/ToDoListTypes";

export const addTaskAction = (newTask) => {
  return {
    type: add_task,
    newTask,
  };
};

export const doneTaskAction = (taskId) => ({
  type: done_task,
  taskId,
});

export const deleteTaskAction = (taskId) => ({
  type: delete_task,
  taskId,
});

export const editTaskAction = (task) => ({
  type: edit_task,
  task,
});
