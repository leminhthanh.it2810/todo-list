import { add_task, delete_task, done_task, edit_task } from "../types/ToDoListTypes";

const initialState = {
  taskList: [
    { id: "task-1", taskName: "task 1", done: true },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
  ],
  taskEdit: {
    id: "task-1",
    taskName: "task 1",
    done: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      // console.log('todo', action.newTask);
      // kiem tra rong
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required");
        return { ...state };
      }
      // kiem tra ton tai
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("task name already exists");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      //  xu ly xong thi gan taskList moi vao taskList
      state.taskList = taskListUpdate;
      return { ...state };
    }

    case done_task: {
      //  click vao button check => dispacth len action co taskId
      let taskListUpdate = [...state.taskList];
      // tu task id tim ra task do o vi tri nao trong mang tien hanh cap nhat lai thuoc tinh done = true. va cap nhat lai state cua redux
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }

    case delete_task: {
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id !== action.taskId
      );
      return { ...state, taskList: taskListUpdate };
    }

    case edit_task: {
      return { ...state, taskEdit: action.task}
    }
    default:
      return { ...state };
  }
};
