import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addTaskAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
} from "./redux/actions/ToDoListActions";
import { add_task } from "./redux/types/ToDoListTypes";

class ToDoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <tr key={index}>
            <td>{task.taskName}</td>
            <td className="text-right">
              <button onClick={() => { 
                this.props.dispatch(editTaskAction(task))
               }}  className="btn btn-primary btn-sm mr-2">
                <i class="fa fa-edit"></i>
              </button>
              <button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="btn btn-success btn-sm mr-2"
              >
                <i className="fa fa-check"></i>
              </button>
              <button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="btn btn-danger btn-sm"
              >
                <i className="fa fa-trash"></i>
              </button>
            </td>
          </tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <tr key={index}>
            <td>{task.taskName}</td>
            <td className="text-right">
              <button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="btn btn-danger btn-sm"
              >
                <i className="fa fa-trash"></i>
              </button>
            </td>
          </tr>
        );
      });
  };

  render() {
    return (
      <div>
        <div className="container">
          <h1 className="mb-4">To do list</h1>
          <div className="mb-4">
            <h5>Task name</h5>
            <div className="input-group">
              <input
              value={this.props.taskEdit.taskName}
                onChange={(e) => {
                  this.setState({
                    taskName: e.target.value,
                  });
                }}
                name="taskName"
                type="text"
                className="form-control"
              />
              <div className="input-group-append">
                <button
                  onClick={() => {
                    // lấy thông tin người dùng nhập vào ô input
                    let { taskName } = this.state;
                    // tạo ra 1 task object
                    let newTask = {
                      id: Date.now(),
                      taskName: taskName,
                      done: false,
                    };
                    console.log(newTask);
                    // đưa task object lên redux thông qua phương thức dispatch

                    this.props.dispatch(addTaskAction(newTask));
                  }}
                  className="btn btn-success"
                  type="button"
                >
                  Add task
                </button>
                <button className="btn btn-primary ml-2" type="button">
                  Update task
                </button>
              </div>
            </div>
          </div>

          <h5>Task to do</h5>
          <table className="table">
            <thead>{this.renderTaskToDo()}</thead>
            <tbody></tbody>
          </table>

          <h5>Task completed</h5>
          <table className="table">
            <thead>{this.renderTaskCompleted()}</thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
